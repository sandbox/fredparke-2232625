<?php
/**
 * @file
 * Template file for the LayerSlider.
 */
?>

<div class="ls-slide" <?php print drupal_attributes($layer_attributes); ?>>
  <?php print $background_image; ?>
  <?php print $layers; ?>
  <?php print $slide_link; ?>
</div>
