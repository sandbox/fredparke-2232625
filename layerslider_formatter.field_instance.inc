<?php
/**
 * @file
 * layerslider.field_instance.inc
 */

/**
 * Custom function generates field instances.
 */
function _layerslider_formatter_installed_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-layerslider_layer-field_ls_layer_image'
  $field_instances['node-layerslider_layer-field_ls_layer_image'] = array(
    'bundle' => 'layerslider_layer',
    'deleted' => 0,
    'description' => 'The image to be used as the slide layer. You may upload an image or use the markup field.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ls_layer_image',
    'label' => 'Layer image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => -4,
    ),
  );

  // Exported field_instance: 'node-layerslider_layer-field_ls_layer_markup'
  $field_instances['node-layerslider_layer-field_ls_layer_markup'] = array(
    'bundle' => 'layerslider_layer',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The markup to be used as the slide layer. Use this markup field or the image field. If the \'Slideshow link\' field has been used on the slide then no links should be put in this field as the entire slide will be a link anyway.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ls_layer_markup',
    'label' => 'Layer markup',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => -3,
    ),
  );

  // Exported field_instance: 'node-layerslider_layer-field_ls_layer_options'
  $field_instances['node-layerslider_layer-field_ls_layer_options'] = array(
    'bundle' => 'layerslider_layer',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Contents of this field will be placed within the data-ls attribute of the slideshow element. It should consist of options for the layer such as <code>offsetxin</code> and <code>delayin</code>. See the <a href="/sites/all/libraries/layerslider/documentation/documentation.html#layer-transitions" target="_blank">documentation here</a>.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ls_layer_options',
    'label' => 'Layer options',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => -1,
    ),
  );

  // Exported field_instance: 'node-layerslider_layer-field_ls_layer_style'
  $field_instances['node-layerslider_layer-field_ls_layer_style'] = array(
    'bundle' => 'layerslider_layer',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Contents of this field will be placed within the style attribute of the slideshow element. This can include general CSS styling for the element and element position (use <code>top</code> and <code>left</code> only). See the <a href="/sites/all/libraries/layerslider/documentation/documentation.html#positioning-layers" target="_blank">documentation here</a>.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ls_layer_style',
    'label' => 'Layer style',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => -2,
    ),
  );

  // Exported field_instance: 'node-layerslider_slide-field_ls_slide_background_color'
  $field_instances['node-layerslider_slide-field_ls_slide_background_color'] = array(
    'bundle' => 'layerslider_slide',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Background colour for the slide. Not needed when using a background image.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ls_slide_background_color',
    'label' => 'Background color',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'jquery_colorpicker',
      'settings' => array(),
      'type' => 'jquery_colorpicker',
      'weight' => -3,
    ),
  );

  // Exported field_instance: 'node-layerslider_slide-field_ls_slide_background_image'
  $field_instances['node-layerslider_slide-field_ls_slide_background_image'] = array(
    'bundle' => 'layerslider_slide',
    'deleted' => 0,
    'description' => 'Background image for the slide - need to be the exact size of the slide.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ls_slide_background_image',
    'label' => 'Background image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => -2,
    ),
  );

  // Exported field_instance: 'node-layerslider_slide-field_ls_slide_layers'
  $field_instances['node-layerslider_slide-field_ls_slide_layers'] = array(
    'bundle' => 'layerslider_slide',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The layers that appear within the slide. These should not contain any links if the \'Slide link\' field has been used.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ls_slide_layers',
    'label' => 'Layers',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => -1,
    ),
  );

  // Exported field_instance: 'node-layerslider_slide-field_ls_slide_link'
  $field_instances['node-layerslider_slide-field_ls_slide_link'] = array(
    'bundle' => 'layerslider_slide',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A link around the entire slide. Please use the node/<nid> syntax when available to ensure the link is always valid.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_ls_slide_link',
    'label' => 'Slide link',
    'required' => 0,
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'user',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => -4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('A link around the entire slide. Please use the node/<nid> syntax when available to ensure the link is always valid.');
  t('Background color');
  t('Background colour for the slide. Not needed when using a background image.');
  t('Background image');
  t('Background image for the slide - need to be the exact size of the slide.');
  t('Contents of this field will be placed within the data-ls attribute of the slideshow element. It should consist of options for the layer such as <code>offsetxin</code> and <code>delayin</code>. See the <a href="/sites/all/libraries/layerslider/documentation/documentation.html#layer-transitions" target="_blank">documentation here</a>.');
  t('Contents of this field will be placed within the style attribute of the slideshow element. This can include general CSS styling for the element and element position (use <code>top</code> and <code>left</code> only). See the <a href="/sites/all/libraries/layerslider/documentation/documentation.html#positioning-layers" target="_blank">documentation here</a>.');
  t('Highlights');
  t('Layer image');
  t('Layer markup');
  t('Layer options');
  t('Layer style');
  t('Layers');
  t('Slide link');
  t('Slides');
  t('The image to be used as the slide layer. You may upload an image or use the markup field.');
  t('The layers that appear within the slide. These should not contain any links if the \'Slide link\' field has been used.');
  t('The markup to be used as the slide layer. Use this markup field or the image field. If the \'Slideshow link\' field has been used on the slide then no links should be put in this field as the entire slide will be a link anyway.');

  return $field_instances;
}
