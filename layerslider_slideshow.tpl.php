<?php
/**
 * @file
 * Template file for the LayerSlider.
 */
?>
<div id="layerslider" <?php print drupal_attributes($element['#slideshow_attributes']); ?>>
  <?php foreach ($items as $delta => $item): ?>
    <?php print render($item); ?>
  <?php endforeach; ?>
</div>
